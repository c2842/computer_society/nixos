{
  config,
  pkgs,
  lib,
  inputs,
  ...
}:
with lib; let
  cfg = config.services.skynet_vaultwarden;

  domain_sub = "pw";
  domain = "${domain_sub}.skynet.ie";
in {
  imports = [
    ../acme.nix
    ../dns.nix
    ../nginx.nix
  ];

  options.services.skynet_vaultwarden = {
    enable = mkEnableOption "Skynet vaultwarden server";

    host = {
      ip = mkOption {
        type = types.str;
      };

      name = mkOption {
        type = types.str;
      };
    };
  };

  config = mkIf cfg.enable {
    #backups = [ "/etc/silver_ul_ical/database.db" ];

    # Website config
    skynet_acme.domains = [
      domain
    ];

    skynet_dns.records = [
      {
        record = domain_sub;
        r_type = "CNAME";
        value = cfg.host.name;
      }
    ];

    services.nginx.virtualHosts."${domain}" = {
      forceSSL = true;
      useACMEHost = "skynet";
      locations."/" = {
        proxyPass = "http://127.0.0.1:${toString config.services.vaultwarden.config.ROCKET_PORT}";
      };
    };

    # has ADMIN_TOKEN and SMTP_PASSWORD
    age.secrets.bitwarden_details.file = ../../secrets/bitwarden/details.age;

    services.vaultwarden = {
      enable = true;

      environmentFile = config.age.secrets.bitwarden_details.path;
      config = {
        DOMAIN = "https://${domain}";
        SENDS_ALLOWED = true;
        SIGNUPS_ALLOWED = false;

        INVITATION_ORG_NAME = "Skyhold";

        ORG_GROUPS_ENABLED = true;

        USE_SENDMAIL = false;

        SMTP_HOST = "mail.skynet.ie";
        SMTP_FROM = "vaultwarden@skynet.ie";
        SMTP_FROM_NAME = "Skynet Bitwarden server";
        SMTP_SECURITY = "starttls";
        SMTP_PORT = 587;

        SMTP_USERNAME = "vaultwarden@skynet.ie";
        SMTP_AUTH_MECHANISM = "Login";
        SMTP_EMBED_IMAGES = true;

        ROCKET_ADDRESS = "127.0.0.1";
        ROCKET_PORT = 8222;

        ROCKET_LOG = "critical";
      };
    };
  };
}
