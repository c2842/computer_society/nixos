{
  config,
  pkgs,
  lib,
  inputs,
  ...
}:
with lib; let
  cfg = config.services.skynet_games;
in {
  imports = [
    ./dns.nix
    ./nginx.nix
    ./games/minecraft.nix
  ];

  options.services.skynet_games = {
    enable = mkEnableOption "Skynet Games";

    host = {
      ip = mkOption {
        type = types.str;
      };

      name = mkOption {
        type = types.str;
      };
    };

    domain = {
      tld = mkOption {
        type = types.str;
        default = "ie";
      };

      base = mkOption {
        type = types.str;
        default = "skynet";
      };

      sub = mkOption {
        type = types.str;
        default = "games";
      };
    };
  };

  config = mkIf cfg.enable {
    skynet_dns.records = [
      # need a base domain
      {
        record = cfg.domain.sub;
        r_type = "CNAME";
        value = cfg.host.name;
      }
    ];

    skynet_acme.domains = [
      "${cfg.domain.sub}.skynet.ie"
    ];

    services.nginx.virtualHosts = {
      "${cfg.domain.sub}.skynet.ie" = {
        forceSSL = true;
        useACMEHost = "skynet";
        root = "${inputs.skynet_website_games.defaultPackage.x86_64-linux}";
      };
    };

    # the minecraft servers
    services.skynet_games_minecraft = {
      enable = true;

      host = {
        ip = cfg.host.ip;
        name = cfg.domain.sub;
      };

      domain = {
        sub = "minecraft.${cfg.domain.sub}";
      };
    };
  };
}
