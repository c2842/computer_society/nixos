{
  config,
  pkgs,
  lib,
  inputs,
  ...
}:
with lib; let
  cfg = config.services.ldap_backend;
  port_backend = "8087";
in {
  imports = [
    ../acme.nix
    ../dns.nix
    ../nginx.nix
    inputs.skynet_ldap_backend.nixosModule."x86_64-linux"
    ../../config/users.nix
  ];

  options.services.ldap_backend = {
    enable = mkEnableOption "Skynet LDAP backend server";

    host = {
      ip = mkOption {
        type = types.str;
      };

      name = mkOption {
        type = types.str;
      };
    };

    domain = {
      tld = mkOption {
        type = types.str;
        default = "ie";
      };

      base = mkOption {
        type = types.str;
        default = "skynet";
      };

      sub = mkOption {
        type = types.str;
        default = "api.account";
      };
    };
  };

  config = mkIf cfg.enable {
    #backups = [ "/etc/silver_ul_ical/database.db" ];

    age.secrets.ldap_details.file = ../../secrets/ldap/details.age;
    age.secrets.ldap_discord.file = ../../secrets/discord/ldap.age;
    age.secrets.ldap_mail.file = ../../secrets/email/details.age;
    age.secrets.ldap_wolves.file = ../../secrets/wolves/details.age;

    skynet_acme.domains = [
      "${cfg.domain.sub}.${cfg.domain.base}.${cfg.domain.tld}"
    ];

    skynet_dns.records = [
      {
        record = cfg.domain.sub;
        r_type = "CNAME";
        value = cfg.host.name;
      }
    ];

    services.nginx.virtualHosts."${cfg.domain.sub}.${cfg.domain.base}.${cfg.domain.tld}" = {
      forceSSL = true;
      useACMEHost = "skynet";
      locations."/".proxyPass = "http://localhost:${port_backend}";
      extraConfig = ''
        add_header Access-Control-Allow-Origin "*";
      '';
    };

    services.skynet_ldap_backend = {
      enable = true;

      # contains teh password in env form
      env = {
        ldap = config.age.secrets.ldap_details.path;
        discord = config.age.secrets.ldap_discord.path;
        mail = config.age.secrets.ldap_mail.path;
        wolves = config.age.secrets.ldap_wolves.path;
      };

      host_port = "127.0.0.1:${port_backend}";
      users = config.skynet.users;
    };
  };
}
