{
  config,
  pkgs,
  lib,
  ...
}:
with lib; let
  cfg = config.services.skynet_ldap_client;

  # always ensure the admin group has access
  create_filter_check_admin = x:
    if !(builtins.elem "skynet-admins" x)
    then x ++ ["skynet-admins"]
    else x;

  # create teh new strings
  create_filter_array = map (x: "(skMemberOf=cn=${x},ou=groups,${cfg.base})");

  create_filter_join = x: concatStringsSep "" x;

  # thought you could escape racket?
  create_filter = x: create_filter_join (create_filter_array (create_filter_check_admin x));

  sudo_create_filter = x: (concatStringsSep ", " (map (x: "cn=${x},ou=groups,${cfg.base}") x));
in {
  # these are needed for teh program in question
  imports = [];

  # give users access to this server
  #services.skynet_ldap_client.groups = ["skynet-users-linux"];

  options.services.skynet_ldap_client = {
    # options that need to be passed in to make this work

    enable = mkEnableOption "Skynet LDAP client";

    address = mkOption {
      type = types.str;
      default = "account.skynet.ie";
      description = lib.mdDoc "The domain the ldap is behind";
    };

    base = mkOption {
      type = types.str;
      default = "dc=skynet,dc=ie";
      description = lib.mdDoc "The base address in the ldap server";
    };

    groups = mkOption {
      type = types.listOf types.str;
      default = [
        "skynet-admins-linux"
      ];
      description = lib.mdDoc "Groups we want to allow access to the server";
    };
    sudo_groups = mkOption {
      type = types.listOf types.str;
      default = [
        "skynet-admins-linux"
      ];
      description = lib.mdDoc "Groups we want to allow access to the server";
    };
  };

  config = mkIf cfg.enable {
    # this is athe actual configuration that we need to do

    security.sudo.extraRules = [
      # admin group has sudo access
      {
        groups = cfg.sudo_groups;
        commands = [
          {
            command = "ALL";
            options = ["NOPASSWD"];
          }
        ];
      }
    ];

    # give users a home dir
    security.pam.services.sshd.makeHomeDir = true;

    services.openssh = {
      # only allow ssh keys
      settings.PasswordAuthentication = false;

      # tell users where tehy cna setup their ssh key
      banner = ''
        If you get 'Permission denied (publickey,keyboard-interactive)' you need to add an ssh key on https://${cfg.address}
      '';
    };

    services.sssd = {
      enable = true;

      sshAuthorizedKeysIntegration = true;

      config = ''
        [domain/skynet.ie]
        id_provider = ldap
        auth_provider = ldap
        sudo_provider = ldap

        ldap_uri = ldaps://${cfg.address}:636

        ldap_search_base = ${cfg.base}
        # thank ye https://medium.com/techish-cloud/linux-user-ssh-authentication-with-sssd-ldap-without-joining-domain-9151396d967d
        ldap_user_search_base = ou=users,${cfg.base}?sub?(|${create_filter cfg.groups})
        ldap_group_search_base = ou=groups,${cfg.base}
        # using commas from https://support.hpe.com/hpesc/public/docDisplay?docId=c02793175&docLocale=en_US
        ldap_sudo_search_base, ${sudo_create_filter cfg.sudo_groups}

        ldap_group_nesting_level = 5

        cache_credentials = false
        entry_cache_timeout = 1

        ldap_user_member_of = skMemberOf

        [sssd]
        config_file_version = 2
        services = nss, pam, sudo, ssh
        domains = skynet.ie

        [nss]
        # override_homedir = /home/%u

        [pam]

        [sudo]

        [autofs]
      '';
    };
  };
}
