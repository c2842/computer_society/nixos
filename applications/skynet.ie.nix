{
  config,
  pkgs,
  lib,
  inputs,
  ...
}:
with lib; let
  cfg = config.services.skynet;
in {
  imports = [
    ./acme.nix
    ./dns.nix
  ];

  options.services.skynet = {
    host = {
      ip = mkOption {
        type = types.str;
      };
      name = mkOption {
        type = types.str;
      };
    };
  };

  config = {
    skynet_acme.domains = [
      # the root one is already covered by teh certificate
      "2016.skynet.ie"
      "discord.skynet.ie"
      "public.skynet.ie"
      "renew.skynet.ie"
    ];

    skynet_dns.records = [
      # means root domain, so skynet.ie
      {
        record = "@";
        r_type = "A";
        value = cfg.host.ip;
      }
      {
        record = "2016";
        r_type = "CNAME";
        value = cfg.host.name;
      }
      {
        record = "discord";
        r_type = "CNAME";
        value = cfg.host.name;
      }
      {
        record = "public";
        r_type = "CNAME";
        value = cfg.host.name;
      }
      {
        record = "renew";
        r_type = "CNAME";
        value = cfg.host.name;
      }
    ];

    networking.firewall.allowedTCPPorts = [80 443];
    services.nginx = {
      enable = true;
      group = "acme";

      virtualHosts = {
        # main site
        "skynet.ie" = {
          forceSSL = true;
          useACMEHost = "skynet";
          root = "${inputs.skynet_website.defaultPackage."x86_64-linux"}";
        };

        # archive of teh site as it was ~2012 to 2016
        "2016.skynet.ie" = {
          forceSSL = true;
          useACMEHost = "skynet";
          root = "${inputs.skynet_website_2016.defaultPackage."x86_64-linux"}";
        };

        # a custom discord url, because we are too cheap otehrwise
        "discord.skynet.ie" = {
          forceSSL = true;
          useACMEHost = "skynet";
          locations."/".return = "307 https://discord.gg/mkuKJkCuyM";
        };

        "public.skynet.ie" = {
          forceSSL = true;
          useACMEHost = "skynet";
          root = "${inputs.compsoc_public.packages.x86_64-linux.default}";
          locations."/".extraConfig = "autoindex on;";
        };

        # for alumni members to renew their account
        "renew.skynet.ie" = {
          forceSSL = true;
          useACMEHost = "skynet";
          root = "${inputs.skynet_website_renew.defaultPackage."x86_64-linux"}";
        };
      };
    };
  };
}
