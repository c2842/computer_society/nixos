{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.services.skynet_ulfm;
in {
  imports = [
    ./acme.nix
    ./dns.nix
    ./firewall.nix
    ./nginx.nix
  ];

  options.services.skynet_ulfm = {
    enable = mkEnableOption "ULFM service";

    host = {
      ip = mkOption {
        type = types.str;
      };

      name = mkOption {
        type = types.str;
      };
    };

    domain = {
      tld = mkOption {
        type = types.str;
        default = "ie";
      };

      base = mkOption {
        type = types.str;
        default = "skynet";
      };

      sub = mkOption {
        type = types.str;
        default = "ulfm";
      };
    };
  };

  config = mkIf cfg.enable {
    # TODO: extract this out into its own config
    age.secrets.ulfm.file = ../secrets/stream_ulfm.age;

    networking.firewall.allowedTCPPorts = [
      8000
    ];

    skynet_acme.domains = [
      "${cfg.domain.sub}.${cfg.domain.base}.${cfg.domain.tld}"
    ];

    skynet_dns.records = [
      {
        record = cfg.domain.sub;
        r_type = "CNAME";
        value = cfg.host.name;
      }
    ];

    skynet_firewall.forward = [
      "ip daddr ${cfg.host.ip} tcp dport 80 counter packets 0 bytes 0 accept"
      "ip daddr ${cfg.host.ip} tcp dport 443 counter packets 0 bytes 0 accept"
      "ip daddr ${cfg.host.ip} tcp dport 8000 counter packets 0 bytes 0 accept"
    ];

    users.groups."icecast" = {};

    users.users."icecast2" = {
      createHome = true;
      isSystemUser = true;
      home = "/etc/icecast2";
      group = "icecast";
    };

    systemd.services.icecast = {
      after = ["network.target"];
      description = "Icecast Network Audio Streaming Server";
      wantedBy = ["multi-user.target"];

      preStart = "mkdir -p /var/log/icecast && chown nobody:nogroup /var/log/icecast";
      serviceConfig = {
        Type = "simple";
        ExecStart = "${pkgs.icecast}/bin/icecast -c /run/agenix/ulfm";
        ExecReload = "${pkgs.coreutils}/bin/kill -HUP $MAINPID";
      };
    };

    services.nginx.virtualHosts."${cfg.domain.sub}.${cfg.domain.base}.${cfg.domain.tld}" = {
      forceSSL = true;
      useACMEHost = "skynet";
      locations."/".proxyPass = "http://localhost:8000";
    };
  };
}
