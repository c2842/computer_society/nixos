{
  config,
  pkgs,
  lib,
  inputs,
  ...
}:
with lib; let
  cfg = config.services.discord_bot;
in {
  imports = [
    inputs.skynet_discord_bot.nixosModule."x86_64-linux"
  ];

  options.services.discord_bot = {
    enable = mkEnableOption "Skynet LDAP backend server";
  };

  config = mkIf cfg.enable {
    #backups = [ "/etc/silver_ul_ical/database.db" ];

    age.secrets.discord_token.file = ../secrets/discord/token.age;
    age.secrets.discord_ldap.file = ../secrets/discord/ldap.age;
    age.secrets.discord_mail.file = ../secrets/email/details.age;
    age.secrets.discord_wolves.file = ../secrets/wolves/details.age;

    services.skynet_discord_bot = {
      enable = true;

      env = {
        discord = config.age.secrets.discord_token.path;
        ldap = config.age.secrets.discord_ldap.path;
        mail = config.age.secrets.discord_mail.path;
        wolves = config.age.secrets.discord_wolves.path;
      };

      discord.server = "689189992417067052";
    };
  };
}
