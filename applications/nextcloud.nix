{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.services.skynet_nextcloud;
  domain = "${cfg.domain.sub}.${cfg.domain.base}.${cfg.domain.tld}";
in {
  imports = [
    ./acme.nix
    ./dns.nix
    ./nginx.nix
    ./nextcloud_.nix
  ];

  options.services.skynet_nextcloud = {
    enable = mkEnableOption "Skynet Nextcloud";

    host = {
      ip = mkOption {
        type = types.str;
      };

      name = mkOption {
        type = types.str;
      };
    };

    domain = {
      tld = mkOption {
        type = types.str;
        default = "ie";
      };

      base = mkOption {
        type = types.str;
        default = "skynet";
      };

      sub = mkOption {
        type = types.str;
        default = "nextcloud";
      };
    };
  };

  config = mkIf cfg.enable {
    # shove the entire config file into secrets

    age.secrets.nextcloud_admin_pass = {
      file = ../secrets/nextcloud/pw.age;
      owner = "nextcloud";
      group = "nextcloud";
    };

    skynet_acme.domains = [
      domain
      "onlyoffice.${domain}"
    ];

    skynet_dns.records = [
      {
        record = cfg.domain.sub;
        r_type = "CNAME";
        value = cfg.host.name;
      }
      {
        record = "onlyoffice.${cfg.domain.sub}";
        r_type = "CNAME";
        value = cfg.host.name;
      }
    ];

    # /var/lib/nextcloud/data

    services.nextcloud = {
      enable = true;
      package = pkgs.nextcloud28;
      hostName = domain;
      https = true;

      configureRedis = true;

      database.createLocally = true;
      config = {
        dbtype = "pgsql";
        defaultPhoneRegion = "IE";
        trustedProxies = ["193.1.99.65"];
        adminpassFile = config.age.secrets.nextcloud_admin_pass.path;
      };

      appstoreEnable = true;

      extraApps = with config.services.nextcloud.package.packages.apps; {
        inherit forms groupfolders mail maps notes onlyoffice polls;
      };

      extraOptions = {
        mail_smtpmode = "sendmail";
        mail_sendmailmode = "pipe";
      };
    };

    nixpkgs.config.allowUnfree = true;
    services.onlyoffice = {
      enable = true;
    };

    services.nginx.virtualHosts = {
      ${domain} = {
        forceSSL = true;
        useACMEHost = "skynet";
      };
      "onlyoffice.${domain}" = {
        forceSSL = true;
        useACMEHost = "skynet";
        locations."/".proxyPass = "http://127.0.0.1:8000";
      };
    };
  };
}
