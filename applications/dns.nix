{
  lib,
  pkgs,
  config,
  nodes,
  ...
}: let
  cfg = config.skynet_dns;

  # reads that date to a string (will need to be fixed in 2038)
  current_date = lib.readFile "${pkgs.runCommand "timestamp" {} "echo -n `date +%s` > $out"}";

  # gets a list of records that match this type
  filter_records_type = r_type: builtins.filter (x: x.r_type == r_type) records;
  filter_records_server = builtins.filter (x: builtins.hasAttr "server" x && x.server) (filter_records_type "A");
  filter_records_a = builtins.filter (x: builtins.hasAttr "server" x && !x.server) (filter_records_type "A");

  process_ptr = records: lib.lists.forEach records (x: process_ptr_sub x);
  process_ptr_sub = record: {
    record = builtins.substring 9 3 record.record;
    r_type = "PTR";
    value = record.value;
  };
  ip_ptr_to_int = ip: lib.strings.toInt (builtins.substring 9 3 ip);

  sort_records_server = builtins.sort (a: b: a.record < b.record) filter_records_server;
  sort_records_a = builtins.sort (a: b: (ip_ptr_to_int a.value) < (ip_ptr_to_int b.value)) filter_records_a;
  sort_records_cname = builtins.sort (a: b: a.value < b.value) (filter_records_type "CNAME");
  sort_records_ptr = builtins.sort (a: b: (lib.strings.toInt a.record) < (lib.strings.toInt b.record)) (process_ptr (filter_records_type "PTR"));
  sort_records_srv = builtins.sort (a: b: a.record < b.record) (filter_records_type "SRV");

  format_records = records: offset: lib.strings.concatMapStrings (x: "${padString x.record offset} IN ${padString x.r_type 5} ${x.value}\n") records;

  # small function to trim it down a tad
  padString = text: length: fixedWidthString_post length " " text;

  # like lib.strings.fixedWidthString but postfix
  fixedWidthString_post = width: filler: str: let
    strw = lib.stringLength str;
    reqWidth = width - (lib.stringLength filler);
  in
    assert lib.assertMsg (strw <= width) "fixedWidthString_post: requested string length (${toString width}) must not be shorter than actual length (${toString strw})";
      if strw == width
      then str
      else (fixedWidthString_post reqWidth filler str) + filler;

  # base config for domains we own (skynet.ie, csn.ul.ie, ulcompsoc.ie)
  get_config_file = (
    domain: ''
      $TTL 60  ; 1 minute
      ; hostmaster@${domain} is an email address that recieves stuff related to dns
      @ IN SOA ${nameserver}.${domain}. hostmaster.${domain}. (
         ; Serial (YYYYMMDDCC) this has to be updated for each time the record is updated
         ${current_date}
         600        ; Refresh (10 minutes)
         300        ; Retry   (5 minutes)
         604800     ; Expire  (1 week)
         3600       ; Minimum (1 hour)
        )

      @ NS  ns1.${domain}.
      @ NS  ns2.${domain}.
        ; @ stands for teh root domain so teh A record below is where ${domain} points to
      ;@ A   193.1.99.76
      ;@ MX  5 ${domain}.

      ; can have multiple mailserves
      @ MX  10 mail.${domain}.


      ; ------------------------------------------
      ; Server Names (A Records)
      ; ------------------------------------------
      ${format_records sort_records_server 31}

      ; ------------------------------------------
      ; A (non server names
      ; ------------------------------------------
      ${format_records sort_records_a 31}

      ; ------------------------------------------
      ; CNAMES
      ; ------------------------------------------
      ${format_records sort_records_cname 31}

      ; ------------------------------------------
      ; TXT
      ; ------------------------------------------
      ${format_records (filter_records_type "TXT") 31}

      ; ------------------------------------------
      ; MX
      ; ------------------------------------------
      ${format_records (filter_records_type "MX") 31}

      ; ------------------------------------------
      ; SRV
      ; ------------------------------------------
      ${format_records sort_records_srv 31}


    ''
  );

  # https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/4/html/reference_guide/s2-bind-configuration-zone-reverse
  # config for our reverse dnspointers (not properly working)
  get_config_file_rev = (
    domain: ''
      $ORIGIN 64-64.99.1.193.in-addr.arpa.
      $TTL 60  ; 1 minute
      ; hostmaster@skynet.ie is an email address that recieves stuff related to dns
      @ IN SOA ${nameserver}.skynet.ie. hostmaster.skynet.ie. (
         ; Serial (YYYYMMDDCC) this has to be updated for each time the record is updated
         ${current_date}
         600        ; Refresh (10 minutes)
         300        ; Retry   (5 minutes)
         604800     ; Expire  (1 week)
         3600       ; Minimum (1 hour)
        )

      @ NS  ns1.skynet.ie.
      @ NS  ns2.skynet.ie.

      ; ------------------------------------------
      ; PTR
      ; ------------------------------------------
      ${format_records sort_records_ptr 3}
    ''
  );

  # domains we dont have proper ownship over, only here to ensure the logs dont get cluttered.
  get_config_file_old_domains = (
    domain: ''
      $TTL 60  ; 1 minute
      ; hostmaster@skynet.ie is an email address that recieves stuff related to dns
      @ IN SOA ${nameserver}.skynet.ie. hostmaster.skynet.ie. (
         ; Serial (YYYYMMDDCC) this has to be updated for each time the record is updated
         ${current_date}
         600        ; Refresh (10 minutes)
         300        ; Retry   (5 minutes)
         604800     ; Expire  (1 week)
         3600       ; Minimum (1 hour)
        )

      @ NS  ns1.skynet.ie.
      @ NS  ns2.skynet.ie.

    ''
  );

  # arrys  of teh two nameservers
  tmp1 = ["193.1.99.109"];
  tmp2 = ["193.1.99.120"];

  primaries = (
    if cfg.server.primary
    then
      # primary servers have no primaries (ones they listen to)
      []
    else if builtins.elem cfg.server.ip tmp1
    then tmp2
    else tmp1
  );

  secondaries = (
    if cfg.server.primary
    then
      if builtins.elem cfg.server.ip tmp1
      then tmp2
      else tmp1
    else []
  );

  # small function to tidy up the spam of the cache networks, would use teh subnet except all external traffic has the ip of teh router
  create_cache_networks = map (x: "193.1.99.${toString x}/32") (lib.lists.range 71 126);

  # standard function to create the etc file, pass in the text and domain and it makes it
  create_entry_etc_sub = domain: text: {
    # Creates /etc/skynet/dns/domain
    "skynet/dns/${domain}" = {
      user = "named";
      group = "named";

      # The UNIX file mode bits
      mode = "0664";

      text = text;
    };
  };
  # (text.owned "csn.ul.ie")

  # standard function to create the etc file, pass in the text and domain and it makes it
  create_entry_etc = domain: type:
    if type == "owned"
    then create_entry_etc_sub domain (text.owned domain)
    else if type == "reverse"
    then create_entry_etc_sub domain (text.reverse domain)
    else if type == "old"
    then create_entry_etc_sub domain (text.old domain)
    else {};

  create_entry_zone = domain: extraConfig: {
    "${domain}" = {
      extraConfig = ''
        ${extraConfig}
        // for bumping the config
        // ${current_date}
      '';
      # really wish teh nixos config didnt use master/slave
      master = cfg.server.primary;
      masters = primaries;
      slaves = secondaries;
      # need to write this to a file
      # using the date in it so it will trigger a restart
      file = "/etc/skynet/dns/${domain}";
      # no leading whitespace for first line
    };
  };

  text = {
    owned = domain: get_config_file domain;
    reverse = domain: get_config_file_rev domain;
    old = domain: get_config_file_old_domains domain;
  };

  extraConfig = {
    owned =
      if cfg.server.primary
      then ''
        allow-update { key rfc2136key.skynet.ie.; };

        dnssec-policy default;
        inline-signing yes;
      ''
      else "";

    # no extra config for reverse
    reverse = "";

    old = "";
  };

  records = builtins.concatLists (
    lib.attrsets.mapAttrsToList (
      key: value: let
        details_server = value.config.skynet_dns.server;
        details_records = value.config.skynet_dns.records;
      in
        if builtins.hasAttr "skynet_dns" value.config
        then
          (
            # got to handle habing a dns record for the dns serves themselves.
            if details_server.enable
            then
              (
                if details_server.primary
                then
                  details_records
                  ++ [
                    {
                      record = "ns1";
                      r_type = "A";
                      value = details_server.ip;
                      server = false;
                    }
                  ]
                else
                  details_records
                  ++ [
                    {
                      record = "ns2";
                      r_type = "A";
                      value = details_server.ip;
                      server = false;
                    }
                  ]
              )
            else details_records
          )
        else []
    )
    nodes
  );

  nameserver =
    if cfg.server.primary
    then "ns1"
    else "ns2";
in {
  imports = [
    ../applications/firewall.nix
  ];

  options = {
    skynet_dns = {
      server = {
        enable = lib.mkEnableOption {
          default = false;
          description = "Skynet DNS server";
          type = lib.types.bool;
        };

        primary = lib.mkOption {
          type = lib.types.bool;
          default = false;
        };

        ip = lib.mkOption {
          type = lib.types.str;
          description = ''
            ip of this server
          '';
        };
      };

      records = lib.mkOption {
        description = "Records, sorted based on therir type";
        type = with lib.types;
          listOf (submodule {
            options = {
              record = lib.mkOption {
                type = str;
              };
              r_type = lib.mkOption {
                type = enum ["A" "CNAME" "TXT" "PTR" "SRV" "MX"];
              };
              value = lib.mkOption {
                type = str;
              };
              server = lib.mkOption {
                description = "Core record for a server";
                type = bool;
                default = false;
              };
            };
          });
      };
    };
  };

  config = lib.mkIf cfg.server.enable {
    # open the firewall for this
    skynet_firewall.forward = [
      "ip daddr ${cfg.server.ip} tcp dport 53 counter packets 0 bytes 0 accept"
      "ip daddr ${cfg.server.ip} udp dport 53 counter packets 0 bytes 0 accept"
    ];

    services.bind.zones =
      (create_entry_zone "csn.ul.ie" extraConfig.owned)
      // (create_entry_zone "skynet.ie" extraConfig.owned)
      // (create_entry_zone "ulcompsoc.ie" extraConfig.owned)
      // (create_entry_zone "64-64.99.1.193.in-addr.arpa" extraConfig.reverse)
      // (create_entry_zone "conradcollins.net" extraConfig.old)
      // (create_entry_zone "edelharty.net" extraConfig.old);

    environment.etc =
      (create_entry_etc "csn.ul.ie" "owned")
      // (create_entry_etc "skynet.ie" "owned")
      // (create_entry_etc "ulcompsoc.ie" "owned")
      // (create_entry_etc "64-64.99.1.193.in-addr.arpa" "reverse")
      // (create_entry_etc "conradcollins.net" "old")
      // (create_entry_etc "edelharty.net" "old");

    # secrets required
    age.secrets.dns_dnskeys = {
      file = ../secrets/dns_dnskeys.conf.age;
      owner = "named";
      group = "named";
    };

    networking.firewall = {
      allowedTCPPorts = [53];
      allowedUDPPorts = [53];
    };

    services.bind = {
      enable = true;

      ipv4Only = true;

      # need to take a look at https://nixos.org/manual/nixos/unstable/#module-security-acme-config-dns
      extraConfig = ''
        include "/run/agenix/dns_dnskeys";
      '';

      # piles of no valid RRSIG resolving 'com/DS/IN' errors
      extraOptions = ''
        dnssec-validation yes;
      '';

      # set the upstream dns servers
      # overrides the default dns servers
      forwarders = [
        # Cloudflare
        "1.1.1.1"
        # Google
        "8.8.8.8"
        # Quad9
        "9.9.9.9"
      ];

      cacheNetworks =
        [
          # this server itself
          "127.0.0.0/24"

          # skynet server in the dmz
          "193.1.96.165/32"
          # all of skynet can use this as a resolver
          /*
          Origianl idea, however all external traffic had the ip of the router
          "193.1.99.64/26"

          So to fix this we need to allow smaller ranges? - Didnt work
          Fallback is explisitly listing each ip we have

          Now have a function for it
          */
        ]
        ++ create_cache_networks;
    };

    systemd.services.bind = {
      # deletes teh journal files evey start so it no longer stalls out
      preStart = ''
        rm -vf /etc/skynet/dns/*.jnl
        rm -vf /etc/skynet/dns/*.jbk
      '';
      restartTriggers = [
        "${config.environment.etc."skynet/dns/skynet.ie".source}"
      ];
    };

    # creates a folder in /etc for the dns to use
    users.groups.named = {};

    users.users.named = {
      createHome = true;
      home = "/etc/skynet/dns";
      group = "named";
      # X11 is to ensure the directory can be traversed
      homeMode = "711";
    };
  };
}
