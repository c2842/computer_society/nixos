{
  config,
  pkgs,
  lib,
  ...
}:
with lib; let
  cfg = config.services.skynet_gitlab_runner;
in {
  imports = [
  ];

  options.services.skynet_gitlab_runner = {
    enable = mkEnableOption "Skynet Gitlab Runner";

    runner = {
      name = mkOption {
        type = types.str;
      };

      gitlab = mkOption {
        default = "https://gitlab.skynet.ie";
        type = types.str;
      };

      description = mkOption {
        default = cfg.runner.name;
        type = types.str;
      };

      docker = {
        image = mkOption {
          default = "alpine:3.18.4";
          type = types.str;
        };

        cleanup_dates = mkOption {
          # https://man.archlinux.org/man/systemd.time.7#CALENDAR_EVENTS
          # it will use a lot of storage so clear it daily, may change to hourly if required
          default = "daily";
          type = types.str;
        };
      };
    };
  };

  config = mkIf cfg.enable {
    # https://search.nixos.org/options?from=0&size=50&sort=alpha_desc&type=packages&query=services.gitlab-runner.
    environment.systemPackages = [
      pkgs.gitlab-runner
    ];

    age.secrets.runner_01_nix.file = ../secrets/gitlab/runners/runner01.age;
    age.secrets.runner_02_general.file = ../secrets/gitlab/runners/runner02.age;

    boot.kernel.sysctl."net.ipv4.ip_forward" = true; # 1
    virtualisation.docker.enable = true;

    # taken from https://github.com/NixOS/nixpkgs/issues/245365#issuecomment-1663854128
    virtualisation.docker.listenOptions = ["/run/docker.sock" "127.0.0.1:2375"];

    services.gitlab-runner = {
      enable = true;

      #      clear-docker-cache = {
      #        enable = true;
      #        dates = cfg.runner.docker.cleanup_dates;
      #      };

      services = {
        # might make a function later to have multiple runners, might never need it though
        runner_nix = {
          cloneUrl = cfg.runner.gitlab;
          description = "For Nix only";
          registrationFlags = ["--docker-host" "tcp://127.0.0.1:2375"];
          registrationConfigFile = config.age.secrets.runner_01_nix.path;
          dockerImage = cfg.runner.docker.image;

          # from https://nixos.wiki/wiki/Gitlab_runner
          dockerVolumes = [
            "/nix/store:/nix/store:ro"
            "/nix/var/nix/db:/nix/var/nix/db:ro"
            "/nix/var/nix/daemon-socket:/nix/var/nix/daemon-socket:ro"
          ];
          dockerDisableCache = true;
          preBuildScript = pkgs.writeScript "setup-container" ''
            mkdir -p -m 0755 /nix/var/log/nix/drvs
            mkdir -p -m 0755 /nix/var/nix/gcroots
            mkdir -p -m 0755 /nix/var/nix/profiles
            mkdir -p -m 0755 /nix/var/nix/temproots
            mkdir -p -m 0755 /nix/var/nix/userpool
            mkdir -p -m 1777 /nix/var/nix/gcroots/per-user
            mkdir -p -m 1777 /nix/var/nix/profiles/per-user
            mkdir -p -m 0755 /nix/var/nix/profiles/per-user/root
            mkdir -p -m 0700 "$HOME/.nix-defexpr"
            . ${pkgs.nix}/etc/profile.d/nix-daemon.sh
            ${pkgs.nix}/bin/nix-channel --add https://nixos.org/channels/nixos-unstable nixpkgs # 3
            ${pkgs.nix}/bin/nix-channel --update nixpkgs
            ${pkgs.nix}/bin/nix-env -i ${concatStringsSep " " (with pkgs; [nix cacert git openssh])}
          '';
          environmentVariables = {
            ENV = "/etc/profile";
            USER = "root";
            NIX_REMOTE = "daemon";
            PATH = "/nix/var/nix/profiles/default/bin:/nix/var/nix/profiles/default/sbin:/bin:/sbin:/usr/bin:/usr/sbin";
            NIX_SSL_CERT_FILE = "/nix/var/nix/profiles/default/etc/ssl/certs/ca-bundle.crt";
          };
          tagList = ["nix"];
        };

        runner_general = {
          cloneUrl = cfg.runner.gitlab;
          description = "General Runner";
          registrationFlags = ["--docker-host" "tcp://127.0.0.1:2375"];
          registrationConfigFile = config.age.secrets.runner_02_general.path;
          dockerImage = cfg.runner.docker.image;
        };
      };
    };
  };
}
