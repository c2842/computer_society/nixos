{
  config,
  pkgs,
  lib,
  inputs,
  ...
}:
with lib; let
  cfg = config.services.skynet_users;
in {
  imports = [
    ./acme.nix
    ./dns.nix
    ./nginx.nix
  ];

  options.services.skynet_users = {
    host = {
      ip = mkOption {
        type = types.str;
      };
      name = mkOption {
        type = types.str;
      };
    };
  };

  config = {
    # ssh access

    # allow more than admins access
    services.skynet_ldap_client = {
      groups = [
        "skynet-admins-linux"
        "skynet-users-linux"
      ];
    };

    # Website config
    skynet_acme.domains = [
      "users.skynet.ie"
      "*.users.skynet.ie"
    ];

    skynet_dns.records = [
      {
        record = "users";
        r_type = "CNAME";
        value = cfg.host.name;
      }
      {
        record = "*.users";
        r_type = "CNAME";
        value = cfg.host.name;
      }
    ];

    environment.systemPackages = [pkgs.vim];

    networking = {
      defaultGateway = {
        address = lib.mkDefault "193.1.96.161";
        interface = lib.mkDefault "eth1";
      };
    };

    # normally services cannot read home dirs
    systemd.services.nginx.serviceConfig.ProtectHome = "read-only";

    services.nginx.virtualHosts = {
      # main site
      "*.users.skynet.ie" = {
        forceSSL = true;
        useACMEHost = "skynet";
        serverName = "~^(?<user>.+)\.users\.skynet\.ie";

        # username.users.skynet.ie/
        # user goes:
        #   chmod 711 ~
        #   chmod -R 755 ~/public_html

        locations."/" = {
          alias = "/home/$user/public_html/";
          index = "index.html";
          extraConfig = "autoindex on;";
          tryFiles = "$uri$args $uri$args/ /index.html";
        };
      };
    };
  };
}
