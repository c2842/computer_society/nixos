{
  config,
  pkgs,
  lib,
  ...
}:
with lib; let
  cfg = config.skynet_acme;
in {
  imports = [];

  options = {
    skynet_acme = {
      domains = lib.mkOption {
        default = [];
        type = lib.types.listOf lib.types.str;
        description = ''
          A list of domains to use for this server.
        '';
      };
    };
  };
  config = {
    # group that will own the certificates
    users.groups.acme = {};

    age.secrets.acme.file = ../secrets/dns_certs.secret.age;

    security.acme = {
      preliminarySelfsigned = false;
      acceptTerms = true;

      defaults = {
        email = "admin_acme@skynet.ie";
        # we use our own dns authorative server for verifying we own the domain.
        dnsProvider = "rfc2136";
        credentialsFile = config.age.secrets.acme.path;
      };

      certs = {
        "skynet" = {
          domain = "skynet.ie";
          extraDomainNames = cfg.domains;
        };
      };
    };
  };
}
