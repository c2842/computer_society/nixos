# Repo Sync
This subdir is intended for syncing repos on <gitlab.skynet.ie> with <gitlab.com>

## CSV file
This file is in the format of local id and remote link.  
It must end on a newline

## Tokens
Tokens have a lifetime of a year.

| Site   | User      | Location                                                          | Scopes | Expiry     |
|--------|-----------|-------------------------------------------------------------------|--------|------------|
| Gitlab | ulcompsoc | https://gitlab.com/-/user_settings/personal_access_tokens         | api    | 2024-12-26 |
| Skynet | compsoc   | https://gitlab.skynet.ie/groups/compsoc1/-/settings/access_tokens | api    | 2024-12-26 |

They are then stored in https://gitlab.skynet.ie/compsoc1/skynet/nixos/-/settings/ci_cd as ``TOKEN`` and ``TOKEN_REMOTE``

After the tokens have been regenerated head to https://gitlab.skynet.ie/compsoc1/skynet/nixos/-/pipelines/new and use the var ``SYNC_OVERRIDE`` with value ``true`` to force an update of all the links.