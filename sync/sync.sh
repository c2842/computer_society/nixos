#!/bin/bash

Get-Existing(){
  curl -s "https://gitlab.skynet.ie/compsoc1/skynet/nixos/-/raw/main/sync/repos.csv" -o "./repos_active.csv"
}

Get-Diff(){
  diff --changed-group-format='%<' --unchanged-group-format='' repos.csv repos_active.csv > repos_diff.csv
}

Mirror-Clear(){
  # existing remotes
  local id=$(tr -d '\n\t\r ' <<<"${1}" )
  local token=$(tr -d '\n\t\r ' <<<"$TOKEN" )
  local response=$(curl -s -X "GET" "https://gitlab.skynet.ie/api/v4/projects/$id/remote_mirrors" --header "PRIVATE-TOKEN: $token")

  # https://stackoverflow.com/a/67638584
  readarray -t my_array < <(jq -c '.[]' <<< $response)
  # iterate through the Bash array
  for item in "${my_array[@]}"; do
    local id_mirror=$(jq --raw-output '.id' <<< "$item")
    curl -s -X "DELETE" "https://gitlab.skynet.ie/api/v4/projects/$id/remote_mirrors/$id_mirror" --header "PRIVATE-TOKEN: $token"
  done
}

Mirror-Create(){
  # make sure the values are clean of extra characters
  local id=$(tr -d '\n\t\r ' <<<"${1}" )
  local REPO_TMP=$(tr -d '\n\t\r ' <<<"${2}" )
  local REPO=${REPO_TMP#"https://"}
  local token=$(tr -d '\n\t\r ' <<<"$TOKEN" )
  local token_remote=$(tr -d '\n\t\r ' <<<"$TOKEN_REMOTE" )

  local body="url=https://oauth2:$token_remote@$REPO&enabled=true&only_protected_branches=false&keep_divergent_refs=false"
  local uri="https://gitlab.skynet.ie/api/v4/projects/$id/remote_mirrors"
  echo $uri
  curl -s -X "POST" "$uri" --header "PRIVATE-TOKEN: $token" --data $body
  # to put output on a new line
  echo ""
}

Main() {
  # for local dev
  source .env

  # if SYNC_OVERRIDE is not set then
  if [ -z "${SYNC_OVERRIDE}" ]; then
      Get-Existing
      Get-Diff
  else
      cp repos.csv repos_diff.csv
  fi

  while IFS="," read -r id remote
  do
    Mirror-Clear $id
    Mirror-Create $id $remote
  done < <(tail -n +2 ./repos_diff.csv)
}

Main