/*

Name:     https://theportalwiki.com/wiki/Wheatley
Why:      Whereever GLaDOS is Wheatly is not too far away
Type:     VM
Hardware: -
From:     2023
Role:     Gitlab Runner
Notes:
*/
{
  pkgs,
  lib,
  nodes,
  ...
}: let
  # name of the server, sets teh hostname and record for it
  name = "wheatly";
  ip_pub = "193.1.99.78";
  hostname = "${name}.skynet.ie";
in {
  imports = [
    ../applications/gitlab_runner.nix
  ];

  deployment = {
    targetHost = hostname;
    targetPort = 22;
    targetUser = null;

    tags = ["active-gitlab"];
  };

  skynet_dns.records = [
    {
      record = name;
      r_type = "A";
      value = ip_pub;
      server = true;
    }
    {
      record = ip_pub;
      r_type = "PTR";
      value = hostname;
    }
  ];

  services.skynet_backup = {
    host = {
      ip = ip_pub;
      name = name;
    };
  };

  services.skynet_gitlab_runner = {
    enable = true;
    runner.name = "runner01";
  };
}
