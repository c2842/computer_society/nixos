/*

Name:     https://en.wikipedia.org/wiki/Optimus_Prime
Why:      Created to sell toys so this vm is for games
Type:     VM
Hardware: -
From:     2023
Role:     Game host
Notes:
*/
{
  pkgs,
  lib,
  nodes,
  arion,
  ...
}: let
  # name of the server, sets teh hostname and record for it
  name = "optimus";
  ip_pub = "193.1.99.112";
  hostname = "${name}.skynet.ie";
in {
  imports = [
    ../applications/games.nix
  ];

  deployment = {
    targetHost = hostname;
    targetPort = 22;
    targetUser = null;

    tags = ["active"];
  };

  skynet_dns.records = [
    {
      record = name;
      r_type = "A";
      value = ip_pub;
      server = true;
    }
    {
      record = ip_pub;
      r_type = "PTR";
      value = hostname;
    }
  ];

  services.skynet_backup = {
    host = {
      ip = ip_pub;
      name = name;
    };
  };

  services.skynet_games = {
    enable = true;
    host = {
      ip = ip_pub;
      name = name;
    };
  };
}
