/*

Name:     https://hitchhikers.fandom.com/wiki/Earth
Why:      Our home(page)
Type:     VM
Hardware: -
From:     2023
Role:     Webserver
Notes:
*/
{
  pkgs,
  lib,
  nodes,
  inputs,
  ...
}: let
  name = "earth";
  ip_pub = "193.1.99.79";
  hostname = "${name}.skynet.ie";
in {
  imports = [
    ../applications/skynet.ie.nix
  ];

  deployment = {
    targetHost = ip_pub;
    targetPort = 22;
    targetUser = null;

    tags = ["active-core"];
  };

  # it has two network devices so two
  skynet_dns.records = [
    {
      record = name;
      r_type = "A";
      value = ip_pub;
      server = true;
    }
    {
      record = ip_pub;
      r_type = "PTR";
      value = hostname;
    }
  ];

  services.skynet_backup = {
    host = {
      ip = ip_pub;
      name = name;
    };
  };

  services.skynet = {
    host = {
      ip = ip_pub;
      name = name;
    };
  };
}
