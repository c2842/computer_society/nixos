/*

Name:     https://masseffect.fandom.com/wiki/Vigil
Why:      Counterpart to Vendetta
Type:     VM
Hardware: -
From:     2023
Role:     DNS Server
Notes:
*/
{
  pkgs,
  lib,
  nodes,
  ...
}: let
  name = "vigil";
  ip_pub = "193.1.99.109";
  hostname = "${name}.skynet.ie";
in {
  imports = [
  ];

  deployment = {
    targetHost = ip_pub;
    targetPort = 22;
    targetUser = null;

    tags = ["active-dns" "dns"];
  };

  services.skynet_backup = {
    host = {
      ip = ip_pub;
      name = name;
    };
  };

  skynet_dns = {
    server = {
      enable = true;
      # secondary dns server (ns2)
      primary = false;
      ip = ip_pub;
    };

    # this server will have to have dns records
    records = [
      # vigil IN A    193.1.99.109
      {
        record = name;
        r_type = "A";
        value = ip_pub;
        server = true;
      }
      # 109   IN PTR  vigil.skynet.ie.
      {
        record = ip_pub;
        r_type = "PTR";
        value = hostname;
      }
    ];
  };
}
