/*

Name:     https://masseffect.fandom.com/wiki/Vendetta
Why:      Vendetta held troves of important data waiting for folks to request it.
Type:     Physical
Hardware: PowerEdge r210
From:     2011 (?)
Role:     DNS Server
Notes:    Using the server that used to be called Earth
*/
{
  pkgs,
  lib,
  nodes,
  ...
}: let
  # name of the server, sets teh hostname and record for it
  name = "vendetta";
  ip_pub = "193.1.99.120";
  hostname = "${name}.skynet.ie";
in {
  imports = [
    ./hardware/RM002.nix
  ];

  networking.hostName = name;

  deployment = {
    targetHost = ip_pub;
    targetPort = 22;
    targetUser = null;

    tags = ["active-dns" "dns"];
  };

  networking = {
    # needs to have an address statically assigned

    defaultGateway.interface = lib.mkForce "eno1";
    interfaces.eno1.ipv4.addresses = [
      {
        address = "193.1.99.120";
        prefixLength = 26;
      }
    ];
  };

  services.skynet_backup = {
    host = {
      ip = ip_pub;
      name = name;
    };
  };

  skynet_dns = {
    server = {
      enable = true;
      # primary dns server (ns1)
      primary = true;
      ip = ip_pub;
    };

    records = [
      # vendetta IN A    193.1.99.120
      {
        record = name;
        r_type = "A";
        value = ip_pub;
        server = true;
      }
      # 120      IN PTR  vendetta.skynet.ie.
      {
        record = ip_pub;
        r_type = "PTR";
        value = hostname;
      }
    ];
  };
}
