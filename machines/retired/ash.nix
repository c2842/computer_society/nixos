/*

Name:     https://en.wikipedia.org/wiki/Ash_(Alien)
Why:      Infilitrate into the network
Type:     VM
Hardware: -
From:     2023
Role:     Wireguard (VPN) Server
Notes:    Thius vpn is for admin use only, to give access to all the servers via ssh
*/
{
  pkgs,
  lib,
  nodes,
  ...
}: let
  # name of the server, sets teh hostname and record for it
  name = "ash";
  ip_pub = "193.1.99.75";
  ip_priv = "172.20.20.5";
  # hostname  = "${name}.skynet.ie";
  hostname = ip_pub;
in {
  imports = [
    # applications for this particular server
    ../applications/firewall.nix
    ../applications/dns.nix
  ];

  deployment = {
    targetHost = hostname;
    targetPort = 22;
    targetUser = null;
  };

  # these two are to be able to add the rules for firewall and dns
  # open the firewall for this
  skynet_firewall.forward = [
    "ip daddr ${ip_pub} udp dport 51820 counter packets 0 bytes 0 accept"
  ];

  skynet_dns.records = {
    external = [
      "${name}       A       ${ip_pub}"
    ];
    cname = [
      #may asw ell add a cname for this
      "wg           CNAME   ${name}"
    ];
  };

  age.secrets.wireguard.file = ../secrets/wireguard.age;

  networking = {
    nat = {
      enable = true;
      externalInterface = "eth0";
      internalInterfaces = ["wg0"];
    };

    firewall = {
      allowedTCPPorts = [22];
      allowedUDPPorts = [8000];
      interfaces.wg0 = {
        allowedTCPPorts = [53];
        allowedUDPPorts = [53];
      };
    };

    wireguard.interfaces.wg0 = {
      # may need to change this to the same base as the full network
      ips = ["172.20.21.0/24"];
      listenPort = 8000;
      privateKeyFile = "/run/agenix/wireguard";

      peers = [
        {
          # silver - Brendan
          publicKey = "46jMR/DzJ4rQCR8MBqLMwcyr2tsSII/xeCjihb6EQgQ=";
          allowedIPs = ["172.20.21.2/32"];
        }
      ];
    };
  };

  environment.systemPackages = [
    # needed to generate keys
    pkgs.wireguard-tools
  ];
}
