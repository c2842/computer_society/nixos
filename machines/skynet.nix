/*

Name:     https://en.wikipedia.org/wiki/Skynet_(Terminator)
Why:      Skynet is eternal
Type:     VM
Hardware: -
From:     2023
Role:     Webserver and member linux box
Notes:    Does not host offical sites
*/
{
  pkgs,
  lib,
  nodes,
  inputs,
  ...
}: let
  name = "skynet";
  # DMZ that ITD provided
  ip_pub = "193.1.96.165";
  # for internal network connectivity
  ip_int = "193.1.99.82";
  hostname = "${name}.skynet.ie";
in {
  imports = [
    ../applications/skynet_users.nix
  ];

  deployment = {
    targetHost = ip_pub;
    targetPort = 22;
    targetUser = null;

    # this one is manually deployed
    tags = ["active-ext"];
  };

  skynet_dns.records = [
    {
      record = name;
      r_type = "A";
      value = ip_pub;
      server = true;
    }
    {
      record = ip_pub;
      r_type = "PTR";
      value = hostname;
    }
  ];

  services.skynet_backup.host = {
    ip = ip_pub;
    name = name;
  };

  services.skynet_users = {
    host = {
      ip = ip_pub;
      name = name;
    };
  };
}
