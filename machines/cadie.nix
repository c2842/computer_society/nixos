/*

Name:     https://en.wikipedia.org/wiki/List_of_Google_April_Fools%27_Day_jokes#CADIE
Why:      CADIE is what google could have been, but they chickened out.
Type:     VM
Hardware: -
From:     2023
Role:     Google but better
Notes:
*/
{
  pkgs,
  lib,
  nodes,
  ...
}: let
  # name of the server, sets teh hostname and record for it
  name = "cadie";
  ip_pub = "193.1.99.77";
  hostname = "${name}.skynet.ie";
in {
  imports = [
    ../applications/nextcloud.nix
  ];

  deployment = {
    targetHost = hostname;
    targetPort = 22;
    targetUser = null;

    tags = ["active"];
  };

  skynet_dns.records = [
    {
      record = name;
      r_type = "A";
      value = ip_pub;
      server = true;
    }
    {
      record = ip_pub;
      r_type = "PTR";
      value = hostname;
    }
  ];

  services.skynet_backup = {
    host = {
      ip = ip_pub;
      name = name;
    };
  };

  services.skynet_nextcloud = {
    enable = true;
    host = {
      ip = ip_pub;
      name = name;
    };
  };
}
