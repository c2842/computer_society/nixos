/*

Name:     https://zim.fandom.com/wiki/GIR
Why:      Gir used to have this role before, servers never die
Type:     VM
Hardware: -
From:     2023
Role:     Email Server
Notes:
*/
{
  pkgs,
  lib,
  nodes,
  ...
}: let
  # name of the server, sets teh hostname and record for it
  name = "gir";
  ip_pub = "193.1.99.76";
  hostname = "${name}.skynet.ie";
  #hostname  = ip_pub;
in {
  imports = [
    ../applications/email.nix
  ];

  deployment = {
    targetHost = hostname;
    targetPort = 22;
    targetUser = null;

    tags = ["active-core"];
  };

  # add this server to dns
  skynet_dns.records = [
    {
      record = name;
      r_type = "A";
      value = ip_pub;
      server = true;
    }
    {
      record = ip_pub;
      r_type = "PTR";
      value = hostname;
    }
  ];

  services.skynet_backup = {
    host = {
      ip = ip_pub;
      name = name;
    };
  };

  # we use this to pass in teh relevent infomation to the
  services.skynet_email = {
    enable = true;
    host = {
      ip = ip_pub;
      name = name;
    };
    domain = "skynet.ie";
  };
}
