/*

Name:     https://en.wikipedia.org/wiki/Galatea_(mythology)
Why:      Created as a product of artistic expression
Type:     VM
Hardware: -
From:     2023
Role:     Icecast server for ULFM
Notes:
*/
{
  pkgs,
  lib,
  nodes,
  config,
  ...
}: let
  # name of the server, sets teh hostname and record for it
  name = "galatea";
  ip_pub = "193.1.99.111";
  hostname = "${name}.skynet.ie";
in {
  imports = [
    ../applications/ulfm.nix
  ];

  deployment = {
    targetHost = hostname;
    targetPort = 22;
    targetUser = null;

    tags = ["active"];
  };

  skynet_dns.records = [
    {
      record = name;
      r_type = "A";
      value = ip_pub;
      server = true;
    }
    {
      record = ip_pub;
      r_type = "PTR";
      value = hostname;
    }
  ];

  services.skynet_backup = {
    host = {
      ip = ip_pub;
      name = name;
    };
  };

  services.skynet_ulfm = {
    enable = true;
    host = {
      ip = ip_pub;
      name = name;
    };
  };
}
