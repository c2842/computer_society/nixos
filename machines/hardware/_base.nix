{
  config,
  options,
  lib,
  ...
}:
with lib; let
  # get a list of interfaces
  interfaces = attrNames config.networking.interfaces;
  # check if an IP has been assigned
  has_ip = interface: (length config.networking.interfaces."${interface}".ipv4.addresses) != 0;
in {
  config = {
    skynet.lxc = false;

    assertions = [
      {
        assertion = lists.any has_ip interfaces;
        message = "Must have a ip address set";
      }
      {
        assertion = config.networking.hostName != "nixos";
        message = "Must have networking.hostName set";
      }
    ];
  };
}
