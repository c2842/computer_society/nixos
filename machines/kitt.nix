/*

Name:     https://en.wikipedia.org/wiki/KITT
Why:      Kitt used to have this role before (as well as email and dns)
Type:     VM
Hardware: -
From:     2023
Role:     LDAP Server
Notes:
*/
{
  pkgs,
  lib,
  nodes,
  ...
}: let
  # name of the server, sets teh hostname and record for it
  name = "kitt";
  ip_pub = "193.1.99.74";
  hostname = "${name}.skynet.ie";
  #hostname  = ip_pub;
in {
  imports = [
    ../applications/ldap/server.nix
    ../applications/discord.nix
    ../applications/bitwarden/vaultwarden.nix
    ../applications/bitwarden/bitwarden_sync.nix
  ];

  deployment = {
    targetHost = hostname;
    targetPort = 22;
    targetUser = null;

    tags = ["active-core"];
  };

  # add this server to dns
  skynet_dns.records = [
    {
      record = name;
      r_type = "A";
      value = ip_pub;
      server = true;
    }
    {
      record = ip_pub;
      r_type = "PTR";
      value = hostname;
    }
  ];

  services.skynet_backup = {
    host = {
      ip = ip_pub;
      name = name;
    };
  };

  services.skynet_ldap = {
    enable = true;
    host = {
      ip = ip_pub;
      name = name;
    };
  };

  services.discord_bot = {
    enable = true;
  };

  services.skynet_vaultwarden = {
    enable = true;

    host = {
      ip = ip_pub;
      name = name;
    };
  };
}
