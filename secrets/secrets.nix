let
  admin = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK6DjXTAxesXpQ65l659iAjzEb6VpRaWKSg4AXxifPw9 Skynet Admin";
  silver_laptop = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFQWfVKls31yK1aZeAu5mCE+xycI9Kt3Xoj+gfvEonDg NixOS Laptop";
  silver_desktop = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN34yTh0nk7HAz8id5Z/wiIX3H7ptleDyXy5bfbemico Desktop";
  thenobrainer = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKjaKI97NY7bki07kxAvo95196NXCaMvI1Dx7dMW05Q1 thenobrainer";

  users = [
    admin
    silver_laptop
    silver_desktop
    thenobrainer
  ];

  agentjones = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDHOxA3uYcqS5gTrG1hS8XXwehzQYAI2I4iULtU8cXft root@agentjones";
  vendetta = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFvcxiSYE38V1IopHj7Z7ZWP1IqnskYCdhj8yCQohVUM root@vendetta";
  vigil = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICDsz1bjNAThqwF48dKIJGOECsCKHTj/Gn5Gh9XyzoSO root@vigil";
  galatea = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAII3Mke5YtaMkLvXJxJ3y7YAIEBesoJk3qJyJsnoLUWgW root@galatea";
  optimus = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIqYbbWy3WWtxvD96Hx+RfTx7fJPPirIEa5bOvUILi9r root@optimus";
  glados = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJ6go7ScvOga9vYqC5HglPfh2Nu8wQTpEKpvIZuMAZom root@glados";
  wheatly = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEehcrWqZbTr4+do1ONE9Il/SayP0xXMvhozm845tonN root@wheatly";
  kitt = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPble6JA2O/Wwv0Fztl/kiV0qj+QMjS+jTTj1Sz8k9xK root@kitt";
  gir = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINL2qk/e0QBqpTQ2xDjF7Cv4c92jJ53jW2fuu88hAF/u root@gir";
  neuromancer = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEFAs6lBJSUBRhtZO3zGKhEIlWvqnHFGAQuQ//9FdAn6 root@neuromancer";
  skynet = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAIFPXJswth8H1k8+zrg8vCnPkfG1hIIa3wR9DBmjpB5 root@skynet";
  earth = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMpvgQcvK7iAm0QrIp5qSvUJzDhOrSBN9MJn9JUSI31I root@earth";
  cadie = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIACcwg27wzzFVvzuTytcnzRmCfGkhULwlHJA/3BeVtgf root@cadie";
  marvin = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIAme2vuVpGYX4La/JtXm3zunsWNDP+SlGmBk/pWmYkH root@marvin";

  systems = [
    agentjones
    vendetta
    vigil
    galatea
    optimus
    glados
    wheatly
    kitt
    gir
    neuromancer
    skynet
    earth
    cadie
    marvin
  ];

  dns = [
    vendetta
    vigil
  ];

  email = [
    gir
  ];

  ldap =
    [
      kitt
    ]
    ++ gitlab
    ++ email;

  gitlab = [
    glados
  ];

  gitlab_runners = [
    wheatly
  ];

  # these need dns stuff
  webservers =
    [
      # ULFM
      galatea
      # Games
      optimus
      # skynet is a webserver for users
      skynet
      # our offical server
      earth
    ]
    # ldap servers are web facing
    ++ ldap
    ++ gitlab
    ++ nextcloud;

  restic = [
    neuromancer
  ];

  discord = [
    kitt
  ];

  nextcloud = [
    cadie
  ];

  bitwarden = [
    kitt
  ];
in {
  # nix run github:ryantm/agenix -- -e secret1.age

  "dns_certs.secret.age".publicKeys = users ++ webservers;
  "dns_dnskeys.conf.age".publicKeys = users ++ dns;

  "stream_ulfm.age".publicKeys = users ++ [galatea];

  "gitlab/pw.age".publicKeys = users ++ gitlab;
  "gitlab/db_pw.age".publicKeys = users ++ gitlab;
  "gitlab/secrets_db.age".publicKeys = users ++ gitlab;
  "gitlab/secrets_secret.age".publicKeys = users ++ gitlab;
  "gitlab/secrets_otp.age".publicKeys = users ++ gitlab;
  "gitlab/secrets_jws.age".publicKeys = users ++ gitlab;
  "gitlab/ldap_pw.age".publicKeys = users ++ gitlab;

  "gitlab/runners/runner01.age".publicKeys = users ++ gitlab_runners;
  "gitlab/runners/runner02.age".publicKeys = users ++ gitlab_runners;

  # for ldap
  "ldap/pw.age".publicKeys = users ++ ldap ++ bitwarden;
  # for use connectring to teh ldap
  "ldap/details.age".publicKeys = users ++ ldap ++ discord ++ bitwarden;

  # everyone has access to this
  "backup/restic.age".publicKeys = users ++ systems;
  "backup/restic_pw.age".publicKeys = users ++ restic;

  # discord bot and discord
  "discord/ldap.age".publicKeys = users ++ ldap ++ discord;
  "discord/token.age".publicKeys = users ++ discord;

  # email stuff
  "email/details.age".publicKeys = users ++ ldap ++ discord;

  # nextcloud
  "nextcloud/pw.age".publicKeys = users ++ nextcloud;

  # handles pulling in data from teh wolves api
  "wolves/details.age".publicKeys = users ++ ldap ++ discord;

  # for bitwarden connector
  "bitwarden/id.age".publicKeys = users ++ bitwarden;
  "bitwarden/secret.age".publicKeys = users ++ bitwarden;
  "bitwarden/details.age".publicKeys = users ++ bitwarden;
}
