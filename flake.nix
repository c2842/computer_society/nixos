{
  description = "Deployment for skynet";

  inputs = {
    # gonna start off with a fairly modern base
    nixpkgs.url = "nixpkgs/nixos-unstable";
    # Return to using unstable once the current master is merged in
    # nixpkgs.url = "nixpkgs/nixos-unstable";

    # utility stuff
    flake-utils.url = "github:numtide/flake-utils";
    agenix.url = "github:ryantm/agenix";
    arion.url = "github:hercules-ci/arion";
    alejandra = {
      url = "github:kamadorueda/alejandra/3.0.0";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # email
    # simple-nixos-mailserver.url = "gitlab:simple-nixos-mailserver/nixos-mailserver";
    simple-nixos-mailserver = {
      type = "gitlab";
      host = "gitlab.skynet.ie";
      owner = "compsoc1%2Fskynet";
      repo = "misc%2Fnixos-mailserver";
    };

    # account.skynet.ie
    skynet_ldap_backend = {
      type = "gitlab";
      host = "gitlab.skynet.ie";
      owner = "compsoc1%2Fskynet";
      repo = "ldap%2Fbackend";
    };
    skynet_ldap_frontend = {
      type = "gitlab";
      host = "gitlab.skynet.ie";
      owner = "compsoc1%2Fskynet";
      repo = "ldap%2Ffrontend";
    };
    skynet_website = {
      type = "gitlab";
      host = "gitlab.skynet.ie";
      owner = "compsoc1%2Fskynet";
      repo = "website%2F2023";
    };
    skynet_website_2016 = {
      type = "gitlab";
      host = "gitlab.skynet.ie";
      owner = "compsoc1%2Fskynet";
      repo = "website%2F2016";
    };
    skynet_website_renew = {
      type = "gitlab";
      host = "gitlab.skynet.ie";
      owner = "compsoc1%2Fskynet";
      repo = "website%2Falumni-renew";
    };
    skynet_website_games = {
      type = "gitlab";
      host = "gitlab.skynet.ie";
      owner = "compsoc1%2Fskynet";
      repo = "website%2Fgames.skynet.ie";
    };
    skynet_discord_bot = {
      type = "gitlab";
      host = "gitlab.skynet.ie";
      owner = "compsoc1%2Fskynet";
      repo = "discord-bot";
    };
    compsoc_public = {
      type = "gitlab";
      host = "gitlab.skynet.ie";
      owner = "compsoc1%2Fcompsoc";
      repo = "presentations%2Fpresentations";
    };
  };

  nixConfig.bash-prompt-suffix = "[Skynet Dev] ";

  outputs = {
    self,
    nixpkgs,
    agenix,
    alejandra,
    ...
  } @ inputs: let
    pkgs = nixpkgs.legacyPackages.x86_64-linux.pkgs;
  in {
    formatter.x86_64-linux = alejandra.defaultPackage."x86_64-linux";

    devShells.x86_64-linux.default = pkgs.mkShell {
      name = "Skynet build env";
      nativeBuildInputs = [
        pkgs.buildPackages.git
        pkgs.buildPackages.colmena
        pkgs.buildPackages.nmap
      ];
      buildInputs = [agenix.packages.x86_64-linux.default];
      shellHook = ''export EDITOR="${pkgs.nano}/bin/nano --nonewlines"; unset LD_LIBRARY_PATH;'';
    };

    colmena = {
      meta = {
        nixpkgs = import nixpkgs {
          system = "x86_64-linux";
          overlays = [];
        };
        specialArgs = {
          inherit inputs;
        };
      };

      # installed for each machine
      defaults = import ./machines/_base.nix;

      # firewall machiene
      agentjones = import ./machines/agentjones.nix;

      # ns1
      vendetta = import ./machines/vendetta.nix;

      # ns2
      vigil = import ./machines/vigil.nix;

      # icecast - ULFM
      galatea = import ./machines/galatea.nix;

      # Game host
      optimus = import ./machines/optimus.nix;

      # LDAP host
      kitt = import ./machines/kitt.nix;

      # Gitlab
      glados = import ./machines/glados.nix;

      # Gitlab runners
      wheatly = import ./machines/wheatly.nix;

      # email
      gir = import ./machines/gir.nix;

      # backup 1
      neuromancer = import ./machines/neuromancer.nix;

      # Skynet, user ssh access
      skynet = import ./machines/skynet.nix;

      # Main skynet sites
      earth = import ./machines/earth.nix;

      # Nextcloud
      cadie = import ./machines/cadie.nix;

      # trainee server
      marvin = import ./machines/marvin.nix;
    };
  };
}
