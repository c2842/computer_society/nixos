# Skynet

This is teh core config for teh skynet cluster which uses [NixOS][1].

## Dev 
### Prep 

1. Install [Nix][2]
2. Enable [Flakes][3]

The system ye use does nto matter much, I (@silver) use nix in wsl and it works grand.

### Shell

Now ye got nix installed and flakes enabled run ``nix develop`` in the root folder (same place this readme is).  
The dev dependencies you need to work with the project will be automatically installed.  
The specific config for this can be found [here][4].

Specifically it installs [Colmena][5] and [Agenix][6].  
Colmena is a build and deployment tool, Agenix is for secret management.

All following commands are inside the shell.
### Colmena

#### Building

To build all nodes (servers) run:
```shell
colmena build
```

To build a specific one
```shell
colmena build --on skynet
```

To build a group (for example the dns servers)
```shell
colmena build --on @active-dns
```

#### Deploy
Deploying is putting (apply-ing) the config tat was built onto the server, there is no need to build first, it will automatically do so.

While the ***recommended way of deploying is using the CI/CD process*** there are times when you will have to manually deploy the config.  
One such case is the ``@active-gitlab`` group if either Gitlab or Gitlab-runner got updated.  
Another is if ye have fecked up DNS.

Your ``~/.ssh/config``  should be set up as follows and you should be a member of ``skynet-admins-linux``

```ini
Host *.skynet.ie 193.1.99.* 193.1.96.165
   User username
   IdentityFile ~/.ssh/skynet/username
   IdentitiesOnly yes
```

Then you can run the following commands like so:

```shell
colmena apply
colmena apply --on @active-dns
colmena apply --on @active-gitlab
```

The CI/CD pipeline has a manual job that can be triggered to update ``@active-gitlab`` if you know it wont cause issues.

### Agenix

Agenix is for storing secrets in an encrypted manner using ssh keys.

All these commands require you to be in the secrets folder ``cd secrets``

#### Prep
1. Go to yer .ssh folder and see if you have a ``id_ed25519`` key ([tutorial][7])
2. Make a pull request to add (``id_ed25519.pub``) to the [secrets config][8].
3. An existing admin will pull, run ``agenix --rekey`` and commit changes.
4. Once committed and pushed up and merged in, you will be able to edit secrets.

``id_ed25519`` is preferred due to its neatness and security (Yes @silver is pedantic.)

#### Editing
When editing a terminal editor will open (nano).  
You must use teh path defined in the ``secrets.nix`` file.

````shell
agenix -e stream_ulfm.age
agenix -e ldap/self_service.age
agenix -e gitlab/runners/runner01.age
````

### Updating inputs
Occasionally you will want to update the inputs for the project.  
It is best to do this every few months or so, there is always a risk of things changing so a small pain often is better than a nightmare if left longer.  
As seen in [this merge request][9] the layout of one config changed which had to be fixed.  

We should be updating ``nixpkgs`` at least once a semester, ideally to teh next NixOS release so we cna show ITD our servers are patched and up to date.

```shell
nix flake lock --update-input nixpkgs
```

### Formatting
Formatting helps keep everything nice and consistent.  
The pipeline will only run if the file is correctly formatted.  

```shell
nix fmt
```





[1]: https://nixos.org/explore
[2]: https://nixos.org/download
[3]: https://nixos.wiki/wiki/Flakes
[4]: https://gitlab.skynet.ie/compsoc1/skynet/nixos/-/blob/main/flake.nix#L33
[5]: https://github.com/zhaofengli/colmena
[6]: https://github.com/ryantm/agenix
[7]: https://docs.gitlab.com/ee/user/ssh.html#see-if-you-have-an-existing-ssh-key-pair
[8]: https://gitlab.skynet.ie/compsoc1/skynet/nixos/-/blob/main/secrets/secrets.nix#L2
[9]: https://gitlab.skynet.ie/compsoc1/skynet/nixos/-/merge_requests/4